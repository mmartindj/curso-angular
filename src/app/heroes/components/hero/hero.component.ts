import { Component } from '@angular/core';

@Component({
  selector: 'app-heroes-hero',
  templateUrl: './hero.component.html',
  styleUrl: './hero.component.css',
})
export class HeroComponent {
  public name: string = 'Ironman';
  public age: number = 45;

  // get method is used like a property
  get capitalizedName(): string {
    return this.name.toUpperCase();
  }

  public getheroDescription(): string {
    return `${this.name} is ${this.age} years old.`;
  }

  public changeHero(): void {
    this.name = 'Spiderman';
  }

  public changeAge(): void {
    this.age = 20;
  }

  public resetData(): void {
    this.name = 'Ironman';
    this.age = 45;
  }
}

import { Component } from '@angular/core';

interface Hero {
  id: number;
  name: string;
  age: number;
  superPower: string;
}

@Component({
  selector: 'app-heroes-list',
  templateUrl: './list.component.html',
  styleUrl: './list.component.css',
})
export class ListComponent {
  public heroes: Hero[] = [
    { id: 1, name: 'Superman', age: 30, superPower: 'fly' },
    { id: 2, name: 'Batman', age: 35, superPower: 'rich' },
    { id: 3, name: 'Wonder woman', age: 25, superPower: 'strong' },
  ];

  public deletedHero: Hero | null = null;

  public deleteLastHero(): void {
    this.deletedHero = this.heroes.pop() || null;
  }
}

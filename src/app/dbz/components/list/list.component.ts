import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Character } from '../../interfaces/character.interface';

@Component({
  selector: 'dbz-list',
  templateUrl: './list.component.html',
})
export class ListComponent {
  @Input()
  public characterList: Character[] = [];

  @Output()
  public onRemoveCharacter: EventEmitter<string> = new EventEmitter();

  emitRemoveCharacter(id: Character['id']): void {
    this.onRemoveCharacter.emit(id);
  }
}
